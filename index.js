const express = require('express');
const nunjuncks = require('nunjucks');
const path = require('path');
const bodyParser = require('body-parser');

const app = express();

nunjuncks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.render('main');
});

function calcularIdade(dtnascimento) {
  const d = new Date();
  const anoAtual = d.getFullYear();
  const mesAtual = d.getMonth() + 1;
  const diaAtual = d.getDate();

  const diaNascimento = dtnascimento.substring(0, 2);
  const mesNascimento = dtnascimento.substring(3, 5);
  const anoNascimento = dtnascimento.substring(6, 10);

  let anos = anoAtual - anoNascimento;

  if (mesAtual < mesNascimento || (mesAtual === mesNascimento && diaAtual < diaNascimento)) {
    anos -= 1;
  }
  return anos < 0 ? 0 : anos;
}

app.post('/check', (req, res) => {
  const { username, dtnascimento } = req.body;
  const idade = calcularIdade(dtnascimento);

  if (idade <= 18) {
    res.redirect(`/minor?nome=${username}`);
  } else {
    res.redirect(`/major?nome=${username}`);
  }
  res.send();
});

app.get('/major', (req, res) => {
  res.render('major', { username: req.query.nome });
});

app.get('/minor', (req, res) => {
  res.render('minor', { username: req.query.nome });
});

app.listen(3000);
